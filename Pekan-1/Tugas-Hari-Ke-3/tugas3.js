// Soal 1

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

// Jawaban Soal 1

var rubahKedua = kataKedua[0].toUpperCase()
var potongKedua = kataKedua.slice(1, 7);
// console.log(rubahKedua);
// console.log(potongKedua);
var kataBaruKedua = rubahKedua + potongKedua
// console.log(kataBaruKedua);
// Senang

var kataBaruKeempat = kataKeempat.toUpperCase();
// console.log(kataBaruKeempat)
// JAVASCRIPT

var hasil = kataPertama + " " + kataBaruKedua + " " + kataKetiga + " " + kataBaruKeempat;
console.log(hasil);
console.log("--------------------------")

// Soal 2

var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

// Jawaban Soal 2

var angkaKeSatu = Number(kataPertama);
var angkaKeDua = Number(kataKedua);
var angkaKeTiga = Number(kataKetiga);
var angkaKeEmpat = Number(kataKeempat);

// console.log(angkaKeSatu)

var hasilHitung = angkaKeSatu + angkaKeDua + angkaKeTiga + angkaKeEmpat
console.log(hasilHitung);
console.log("--------------------------")

// Soal 3

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 31); 

// Jawaban Soal 3

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
console.log("--------------------------")

// Soal 4

// var nilai;
// nilai >= 80 indeksnya A
// nilai >= 70 dan nilai < 80 indeksnya B
// nilai >= 60 dan nilai < 70 indeksnya c
// nilai >= 50 dan nilai < 60 indeksnya D
// nilai < 50 indeksnya E

// Jawaban Soal 4

// isi variabel tersebut dengan angka dari 0 sampai 100
var nilai = 98;

if (nilai >= 80) {
    console.log("indeksnya A");
  } else if (nilai >=70 && nilai < 80) {
    console.log("indeksnya B");
  } else if (nilai >=60 && nilai < 70) {
    console.log("indeksnya C");
  } else if (nilai >=50 && nilai < 60) {
    console.log("indeksnya D");
  } else {
    console.log("indeksnya E");
  }

  console.log("--------------------------")
//   Soal 5

var tanggal = 16;
var bulan = 8;
var tahun = 1993;

// Jawaban Soal 5


switch (bulan) {  
    case 1: m = 'Jan'; break;
    case 2: m = 'Feb'; break;
    case 3: m = 'Mar'; break;
    case 4: m = 'Apr'; break;
    case 5: m = 'May'; break;
    case 6: m = 'Jun'; break;
    case 7: m = 'Jul'; break;
    case 8: m = 'Aug'; break;
    case 9: m = 'Sep'; break;
    case 10: m = 'Oct'; break;
    case 11: m = 'Nov'; break;
    case 12: m = 'Dec'; break;
    default: break;
}

var hasilTanggal = tanggal + " " + m + " " + tahun

console.log(hasilTanggal)
console.log("--------------------------")
