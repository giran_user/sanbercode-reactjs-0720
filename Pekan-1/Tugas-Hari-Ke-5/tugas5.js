// Soal 1
// Tulislah sebuah function dengan nama halo() yang mengembalikan nilai 
// “Halo Sanbers!” yang kemudian dapat ditampilkan di console.

// Jawaban Soal 1

function halo(){
    return '"Halo Sanbers!"'
}

console.log(halo()) 
console.log("-----------------------------------")

// Soal 2
// Tulislah sebuah function dengan nama kalikan() yang mengembalikan 
// hasil perkalian dua parameter yang di kirim.

// Jawaban 2

function kalikan(angka1=0, angka2=0){
    return angka1 * angka2
}
 
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48
console.log("-----------------------------------")

// Soal 3
// Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: 
// “Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!”


// Jawaban Soal 3

function introduce(name="", age=0, address="", hobby=""){
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu " + hobby + "!"
}
 
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!"
console.log("-----------------------------------")



