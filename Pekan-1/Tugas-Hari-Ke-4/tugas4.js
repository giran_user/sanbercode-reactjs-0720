// Soal 1
// LOOPING PERTAMA
// 2 - I love coding
// 4 - I love coding
// 6 - I love coding
// 8 - I love coding
// 10 - I love coding
// 12 - I love coding
// 14 - I love coding
// 16 - I love coding
// 18 - I love coding
// 20 - I love coding
// LOOPING KEDUA
// 20 - I will become a frontend developer
// 18 - I will become a frontend developer                                                                              
// 16 - I will become a frontend developer
// 14 - I will become a frontend developer
// 12 - I will become a frontend developer
// 10 - I will become a frontend developer
// 8 - I will become a frontend developer
// 6 - I will become a frontend developer
// 4 - I will become a frontend developer
// 2 - I will become a frontend developer

// Jawaban Soal 1

var i = 1;
console.log("LOOPING PERTAMA")
while (i < 22) {
    if (i %2 === 0) {
        console.log(i + ' - I love coding');        
    }
    i++;
}

var i = 20;
console.log("LOOPING KEDUA")
while (i > 0) {
    if (i %2 === 0) {
        console.log(i + ' - I will become a frontend developer');        
    }
    i--;
}
console.log("--------------------------------------")

// Soal 2
// A. Jika angka ganjil maka tampilkan Santai
// B. Jika angka genap maka tampilkan Berkualitas
// C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.

// Jawaban Soal 2

for (let i = 1; i < 21; i++) {
    if (i %3 == 0 && i %2 != 0) {
        console.log(i + " - I Love Coding");        
    } else if (i %2 != 0) {
        console.log(i + " - Santai");        
    } else if (i %2 == 0) {
        console.log(i + " - Berkualitas")
    } 
}
console.log("--------------------------------------")

// Soal 3
// #
// ##
// ###
// ####
// #####
// ######
// #######

// Jawaban Soal 3

let z='';
for (let i=0; i<=6; i++){
	for (let j=0; j<=i ;j++){
            z += ' #'
        }
        z += '\n'
    }
console.log(z)
console.log("--------------------------------------")

// Soal 4

// var kalimat="saya sangat senang belajar javascript"
// Rubah menjadi
// ["saya", "sangat", "senang", "belajar", "javascript"]

// Jawaban Soal 4
var kalimat = "saya sangat senang belajar javascript"
var hasil = kalimat.split(" ")

console.log(hasil)
console.log("--------------------------------------")

// Soal 5
// var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
// 1. Mangga
// 2. Apel
// 3. Anggur
// 4. Semangka
// 5. Jeruk


// Jawaban Soal 5

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var sortingBuah = daftarBuah.sort()
// console.log(sortingBuah)

tampung = ''
for (let i = 0; i < sortingBuah.length; i++) {
    tampung += sortingBuah[i] + '\n'
    
}
console.log(tampung)
console.log("--------------------------------------")