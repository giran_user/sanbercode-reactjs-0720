
// // ----------------------------------

// var dataFilm = []

// function dataFilmPush(){
//     return dataFilm.push(objFilm)
// }

// var objFilm = {
//     nama : "Avengers",
//     durasi: "2 Jam",
//     genre: "action",
//     tahun: 2012
// }

// var hasilFilm = dataFilmPush()

// // console.log(hasilFilm) 
// // console.log("-----------------------------------")

// function introduce(name="", age=0, address="", hobby=""){
//     return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu " + hobby + "!"
// }
 
// var name = "John"
// var age = 30
// var address = "Jalan belum jadi"
// var hobby = "Gaming"
 
// var perkenalan = introduce(name, age, address, hobby)
// // console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!"
// // console.log("-----------------------------------")


// var nietos = [];
// var obj = {};
// obj["nama"] = "avengers";
// obj["durasi"] = "2 Jam", "3 Jam";
// nietos.push(obj);

// // console.log(nietos)


// // LATIHAN

// // var personArr = ["John", "Doa", "male", 27]
// // console.log(personArr[0])

// // var personObj = {
// //     firstName : "John",
// //     lastName : "Doe",
// //     gender: "male",
// //     age: 27
// // }
// // console.log(personObj.firstName)

// // var object = {
// //     [key]: [value]
// // }

// // var car = {
// //     brand:"Ferrari",
// //     type: "Sports Car",
// //     price: 50000000,
// //     "horse power": 986
// // }

// // var car2 = {}
// // // meng-assign key:value dari object cer2
// // car2.brand = "Lamborghini"
// // car2.brand = "Sport Car"
// // car2.price = 100000000
// // car2["horse power"] = 730

// // console.log(car2)

// // var motorcycle1 = {
// //     brand: "Handa",
// //     type : "CUB",
// //     price: 1000
// // }
// // console.log(motorcycle1.brand)
// // console.log(motorcycle1.type)

// // console.log(motorcycle1["price"])

// // var array = [1, 2, 3]
// // console.log(typeof array)

// // Array of Object

// // var mobil = [
// //     {merk: "BMW", warna: "merah", tipe: "sedan"},
// //     {merk: "toyota", warna: "hitam", tipe: "box"},
// //     {merk: "audi", warna: "biru", tipe: "sedan"}
// // ]

// // console.log(mobil)

// // Perulangan atau iteration dalam array of object
// // array.forEach(element => {
    
// // });

// // Map 

// // filter() 

// // foreach()
// // var mobil =[
// //     {id: 1, merk: "BWM", warna: "merah", tipe: "sedan"},
// //     {id: 2, merk: "toyota", warna: "hitam", tipe: "box"},
// //     {id: 3, merk: "audi", warna: "biru", tipe: "sedan"},
// // ]

// // var arrayBaru = mobil.filter(function(item){
// //     return item == item[0];
// // })

// // console.log(arrayBaru)

// // var arrayBaru = mobil.map(function(item){
// //     return item
// // })

// // console.log(arrayBaru)

// // mobil.forEach(function(item){
// //     console.log("warna: " + item.warna)
// // })

// // var arrayWarna = mobil.map(function(item){
// //     return item.warna

// // })

// // console.log(arrayWarna)

// // var arrayMobilFilter = mobil.filter(function(item){
// //     return item.tipe != "sedan";
// // })

// // var arrayMobilFilter = mobil.filter(function(item){
// //     return item.tipe != "sedan";
// // })

// // console.log(arrayMobilFilter)


// function introduce(name="", age=0, address="", hobby=""){
//     return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu " + hobby + "!"
// }
 
// var name = "John"
// var age = 30
// var address = "Jalan belum jadi"
// var hobby = "Gaming"
 
// var perkenalan = introduce(name, age, address, hobby)
// console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!"
// console.log("-----------------------------------")


// LATIHAN CLASS

// class Car {
//     constructor(brand, factory) {
//         this.brand = brand 
//         this.factory = factory
//         this.sound = "honk! honk!vroomvroom"
        
//     }
    
// }

// var Car = class {
//     constructor(brand, factory) {
//         this.brand = brand 
//         this.factory = factory
        
//     }
    
// }

// console.log(Car.name)

// var Car = class Car2 {
//     constructor(brand, factory) {
//         this.brand = brand 
//         this.factory = factory
        
//     }
    
// }

// console.log(Car.name)

// class Car {
//     constructor(brand) {
//         this.carname = brand;        
//     }

//     present(){
//         return "I have a " + this.carname;
//     }
    
// }

// mycar = new Car("Ford");
// console.log(mycar.present())


// METHOD
// class Car {
//     constructor(brand) {
//         this.carname = brand;        
//     }
//     present(x){
//         return x + ", I Have a " + this.carname;
//     }
    
// }

// mycar = new Car("Ford");
// console.log(mycar.present("Hello"));

// STATIC METHOD

// class Car {
//     constructor(brand) {
//         this.carname = brand;
        
//     }

//     static hello(){
//         return "Hello!!";
//     }
    
// }

// mycar = new Car("Ford");

// // memanggil "hello()" pada class Car:
// console.log(Car.hello());

// INHERITANCE (PENTING)

class Car {
    constructor(brand){
        this.carname = brand;
    }

    present(){
        return "I have a " + this.carname;
    }
}

class Model extends Car {
    constructor(brand, mod){
        super(brand);
        this.model = mod;
    }

    show(){
        return this.present() + ", it is a " + this.model;
    }
}

mycar = new Model("Ford", "Mustang");
console.log(mycar.show());

// GETTERS dan SETTERS

// class Car {
//     constructor(brand){
//         this._carname = brand;
//     }

//     get carname1(){
//         return this._carname;
//     }

//     // set carname(x){
//     //     this._carname = x;
//     // }
// }

// mycar = new Car("Ford");
// // mycar.carname = "Volvo"
// console.log(mycar.carname1)