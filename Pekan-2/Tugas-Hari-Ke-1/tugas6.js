// Soal 1
// var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]

// Jawaban 1

var ObjDaftarPeserta = {
    nama: "Asep",
    "jenis kelamin": "laki-laki",
    hobi: "baca buku",
    "tahun lahir": 1992
}

console.log(ObjDaftarPeserta)
console.log("-----------------------------------")



// Soal 2
// 1.nama: strawberry
//   warna: merah
//   ada bijinya: tidak
//   harga: 9000 
// 2.nama: jeruk
//   warna: oranye
//   ada bijinya: ada
//   harga: 8000
// 3.nama: Semangka
//   warna: Hijau & Merah
//   ada bijinya: ada
//   harga: 10000
// 4.nama: Pisang
//   warna: Kuning
//   ada bijinya: tidak
//   harga: 5000

// Jawaban 2

var buah = [
    {id: 1, nama: "strawberry", warna: "merah", "ada bijinya": "tidak" ,harga: 9000},
    {id: 2, nama: "jeruk", warna: "oranye", "ada bijinya": "ada" ,harga: 8000},
    {id: 3, nama: "Semangka", warna: "Hijau & Merah", "ada bijinya": "ada" ,harga: 10000},
    {id: 4, nama: "Pisang", warna: "Kuning", "ada bijinya": "tidak" ,harga: 5000}, 
   
]

var arrayBuahFilter = buah.filter(function(item){
    return item.id == 1;
})

console.log(arrayBuahFilter)
console.log("-----------------------------------")


// Soal 3
// buatlah fungsi untuk menambahkan dataFilm tersebut yang 
// itemnya object adalah object dengan property nama, durasi , genre, tahun

// Jawaban Soal 3

var dataFilm = []

var objFilm = {
    nama : "Avengers",
    durasi: "2 Jam",
    genre: "action",
    tahun: 2012
}

function pushDataFilm(objFilm={}){
    return dataFilm.push(objFilm)
}

var hasil = pushDataFilm(objFilm)

console.log(dataFilm)
console.log("-----------------------------------")

// Soal 4

// class Animal {
//     // Code class di sini
// }
 
// var sheep = new Animal("shaun");
 
// console.log(sheep.name) // "shaun"
// console.log(sheep.legs) // 4
// console.log(sheep.cold_blooded) // false

// Code class Ape dan class Frog di sini
 
// var sungokong = new Ape("kera sakti")
// sungokong.yell() // "Auooo"
 
// var kodok = new Frog("buduk")
// kodok.jump() // "hop hop" 

// Jawaban Soal 4

// Release 0

class Animal {
    constructor(name){
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }

    get name(){
        return this._name;
    }
    
    get legs(){
        return this._legs;
    }

    get cold_blooded(){
        return this._cold_blooded;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log("=========")

// Release 1

// Code class Ape dan class Frog di sini

class Ape extends Animal {
    constructor(name){
        super(name);
        this._name = name;
        this._legs = 4;
    }

    yell(){
        return "Auooo"
    }  

    get legs(){
        return this._legs;
    }

    set legs(x){
        return this._legs = x;
    }
}
 
var sungokong = new Ape("kera sakti");
sungokong.legs = 2;
console.log(sungokong.name);
console.log(sungokong.legs);
console.log(sungokong.yell()); // "Auooo"
console.log("=========")


class Frog extends Animal {
    constructor(name){
        super(name);
        this._name = name;
        this._legs = 4;
    }

    jump(){
        return "hop hop"
    }  
}
 
var kodok = new Frog("buduk")
console.log(kodok.name);
console.log(kodok.legs);
console.log(kodok.jump()); // "hop hop"
console.log("-----------------------------------")


// Soal 5
// function Clock({ template }) {
  
//     var timer;
  
//     function render() {
//       var date = new Date();
  
//       var hours = date.getHours();
//       if (hours < 10) hours = '0' + hours;
  
//       var mins = date.getMinutes();
//       if (mins < 10) mins = '0' + mins;
  
//       var secs = date.getSeconds();
//       if (secs < 10) secs = '0' + secs;
  
//       var output = template
//         .replace('h', hours)
//         .replace('m', mins)
//         .replace('s', secs);
  
//       console.log(output);
//     }
  
//     this.stop = function() {
//       clearInterval(timer);
//     };
  
//     this.start = function() {
//       render();
//       timer = setInterval(render, 1000);
//     };
  
//   }
  
//   var clock = new Clock({template: 'h:m:s'});
//   clock.start(); 

// Jawaban Soal 5

class Clock {
    constructor({ template }) {
      this.template = template;
    }
  
    render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    }
  }

var clock = new Clock({template: 'h:m:s'});
clock.start(); 


 
