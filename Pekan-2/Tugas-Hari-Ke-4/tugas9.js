// Soal 1
// return dalam fungsi di bawah ini masih menggunakan object literal dalam ES5, 
// ubahlah menjadi bentuk yang lebih sederhana di ES6.

// Code ES5
// const newFunction = function literal(firstName, lastName){
//     return {
//       firstName: firstName,
//       lastName: lastName,
//       fullName: function(){
//         console.log(firstName + " " + lastName)
//         return 
//       }
//     }
//   }
   
  //Driver Code 
//   newFunction("William", "Imoh").fullName() 

// Jawaban 1

// Setelah di rubah jadi Code Version ES6
const newFunction = literal = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName(){
            console.log(firstName + " " + lastName)
            return 
        }
    }       

}
newFunction("William", "Imoh").fullName() 
console.log("--------------------------")

// Soal 2
// Gunakan metode destructuring dalam ES6 untuk 
// mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)

// Jawaban Soal 2
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation)
console.log("--------------------------")

// Soal 3
// Kombinasikan dua array berikut menggunakan array spreading ES6

// Jawaban Soal 3
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined)
console.log("--------------------------")


////////////////////////////////////

// LATIHAN

// spread + rest

// let buah = ["mangga", "jeruk"]

// buah = ["semangka", ...buah]

// let person = {
//     name : "John",
//     job : "programmer",
//     height : 175,
//     weight : 55
// }

// person = {...person, favouriteFruits: [...buah, "anggur"]}

// console.log(person)


// rest

// let scores = ["98", "95", "93", "90", "87", "85"]
// let [first, second, third, ...restOfScores] = scores;

// console.log(scores);

// const luasPersegiPanjang = (...rest) => {
//     const [panjang, lebar] = rest 
//     return panjang * lebar

// }

// console.log(luasPersegiPanjang(2, 10))