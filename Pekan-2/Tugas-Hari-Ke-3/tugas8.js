// Soal 1
// buatlah fungsi menggunakan arrow function luas lingkaran dan keliling lingkaran 
// dengan arrow function lalu gunakan let dan const di dalam soal ini

// Jawaban Soal 1

let r = 14
const phi = Math.PI

let luasLingkaran = (r=0) => {
    return phi * r * r
}

let hasilLuas = luasLingkaran(r)
console.log(hasilLuas) 
console.log("=================")

let kelilingLingkaran = (r=0) => {
    return 2 * phi * r
}

let hasilKeliling = kelilingLingkaran(r)
console.log(hasilKeliling) 
console.log("-----------------------------------")


// Soal 2
// let kalimat = ""
// buatlah fungsi menambahkan kata di kalimat dan 
// gunakan penambahan kata tersebut dengan template literal, 
// berikut ini kata kata yang mesti di tambahkan

// saya
// adalah
// seorang
// frontend
// developer


// Jawaban Soal 2
let kalimat = ""

let kata1 = "saya"
let kata2 = "adalah"
let kata3 = "seorang"
let kata4 = "frontend"
let kata5 = "developer"

let tambahKalimat = (kata1="", kata2="", kata3="", kata4="", kata5="",) => {
    kalimat = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`
    return kalimat
}  

console.log(tambahKalimat(kata1, kata2, kata3, kata4, kata5,))
console.log("-----------------------------------")

// Soal 3
// buatlah class Book yang didalamnya terdapat property name, totalPage, price. lalu buat class baru komik yang extends terhadap buku dan 
// mempunyai property sendiri yaitu isColorful yang isinya true atau false

// Jawaban Soal 3

class Book {
    constructor(name){
        this._name = name;
        this._totalPage = 200;
        this._price = 120000;
    }

    get name(){
        return this._name;
    }
    
    get totalPage(){
        return this._totalPage;
    }

    get price(){
        return this._price;
    }
}
 
var koding = new Book("Belajar JavaScript Pemula");
 
console.log("Book : " + koding.name) 
console.log("Total Page : " + koding.totalPage) 
console.log("Price : " + koding.price) 
console.log("=========")

class Komik extends Book {
    constructor(name){
        super(name);
        this._totalPage = 100;
        this._price = 50000;
        this._isColorful = false;
    }
 

    get isColorful(){
        return this._isColorful;
    }
}
 
var bukuKomik = new Komik("One Piece");
console.log("Book : " + bukuKomik.name) 
console.log("Total Page : " + bukuKomik.totalPage) 
console.log("Price : " + bukuKomik.price)
console.log("Is Colorful : " + bukuKomik.isColorful)
console.log("=========")
console.log("-----------------------------------")





/////////////////////////////////////////////////////

// LATIHAN
// let + Const
// arrow function
// default parameter
// template literal
// class

// Normal Javascript

// var x = 1;
// if (x === 1) {
//     var x = 2;

// console.log(x);
// }

// console.log(x)

// ES6

// let x = 1;

// if (x === 1) {
//     let x = 2;

//     console.log(x);
// }

// console.log(x);


// const number = 42;
// number = 100;

// Normal Function

// function f (){
//     // isi function
// }

// var f = function (){
//     // isi function
// }

// // arrow function

// var f = () => {
//     // function
//     return () =>{
//         // returning a function
//     }
// }